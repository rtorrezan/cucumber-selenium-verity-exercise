$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("features/Contato.feature");
formatter.feature({
  "name": "Realizar cadastro de Contato no Site",
  "description": "",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "name": "Realizar cadastro de contato com sucesso",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "que eu acessei a home page da Verity Group",
  "keyword": "Given "
});
formatter.step({
  "name": "eu clicar na opcao de menu \"Contato\"",
  "keyword": "When "
});
formatter.step({
  "name": "preencher os campos Empresa: \"\u003cempresa\u003e\", Nome: \"\u003cnome\u003e\", Email: \"\u003cemail\u003e\", Telefone: \"\u003ctelefone\u003e\", Assunto: \"\u003cassunto\u003e\" e Mensagem: \"\u003cmensagem\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "clicar no botao \"Enviar\"",
  "keyword": "And "
});
formatter.step({
  "name": "o site ira registrar o envio apresentando uma mensagem de que a mensagem foi enviada com sucesso.",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "empresa",
        "nome",
        "email",
        "telefone",
        "assunto",
        "mensagem"
      ]
    },
    {
      "cells": [
        "Rtorrezan",
        "ricardo silva torrezan",
        "rtorrezan@gmail.com",
        "11981012393",
        "Dúvidas",
        "teste vaga QA"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Realizar cadastro de contato com sucesso",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "que eu acessei a home page da Verity Group",
  "keyword": "Given "
});
formatter.match({
  "location": "ContatoSD.que_eu_acessei_a_home_page_da_Verity_Group()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "eu clicar na opcao de menu \"Contato\"",
  "keyword": "When "
});
formatter.match({
  "location": "ContatoSD.eu_clicar_na_op_o_de_menu_Contato()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "preencher os campos Empresa: \"Rtorrezan\", Nome: \"ricardo silva torrezan\", Email: \"rtorrezan@gmail.com\", Telefone: \"11981012393\", Assunto: \"Dúvidas\" e Mensagem: \"teste vaga QA\"",
  "keyword": "And "
});
formatter.match({
  "location": "ContatoSD.preencher_os_campos(String,String,String,String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "clicar no botao \"Enviar\"",
  "keyword": "And "
});
formatter.match({
  "location": "ContatoSD.clicar_no_bot_o_Enviar()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "o site ira registrar o envio apresentando uma mensagem de que a mensagem foi enviada com sucesso.",
  "keyword": "Then "
});
formatter.match({
  "location": "ContatoSD.o_site_ir_registrar_o_envio_apresentando_uma_mensagem_de_que_a_mensagem_foi_enviada_com_sucesso()"
});
formatter.result({
  "status": "passed"
});
});