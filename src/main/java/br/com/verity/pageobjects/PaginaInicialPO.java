package br.com.verity.pageobjects;

import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;


@DefaultUrl("http://www.verity.com.br")   
public class PaginaInicialPO extends PageObject {
	@FindBy(xpath = "//A[contains(text(),'Contato')][1]")
	public WebElement menuContato;
	
	
	
}
