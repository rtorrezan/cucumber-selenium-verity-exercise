package br.com.verity.pageobjects;

import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;


@DefaultUrl("http://www.verity.com.br")   
public class ContatoPO extends PageObject {
	@FindBy(name = "empresa")
	public WebElement txtEmpresa;
	
	@FindBy(name = "nome")
	public WebElement txtNome;
	
	@FindBy(name = "email")
	public WebElement txtEmail;
	
	@FindBy(name = "telefone")
	public WebElement txtTelefone;
	
	@FindBy(xpath = "/html/body/div/main/div[2]/section/div/form/label[5]/select")
	public WebElement txtAssunto;
	
	@FindBy(name = "mensagem")
	public WebElement txtMensagem;
	
	@FindBy(xpath = "//BUTTON[contains(text(),'Enviar')]")
	public WebElement btnEnviar;
	
	@FindBy(xpath = "//DIV[@class='button']")
	public WebElement msgEnviado;
	
}
