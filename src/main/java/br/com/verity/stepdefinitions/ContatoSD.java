package br.com.verity.stepdefinitions;

import org.junit.Assert;
import org.openqa.selenium.By;

import br.com.verity.pageobjects.ContatoPO;
import br.com.verity.pageobjects.PaginaInicialPO;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ContatoSD {
	ContatoPO contatoPO;
	PaginaInicialPO paginaInicialPO;

	@Given("^que eu acessei a home page da Verity Group$")
	public void que_eu_acessei_a_home_page_da_Verity_Group() {
		paginaInicialPO.open();
	}

	@When("^eu clicar na opcao de menu \"Contato\"$")
	public void eu_clicar_na_op_o_de_menu_Contato() {
		paginaInicialPO.menuContato.click();
	}

	@And("^preencher os campos Empresa: \"([^\"]*)\", Nome: \"([^\"]*)\", Email: \"([^\"]*)\", Telefone: \"([^\"]*)\", Assunto: \"([^\"]*)\" e Mensagem: \"([^\"]*)\"$")
	public void preencher_os_campos(String strEmpresa, String strNome, String strEmail, String strTelefone,
			String strAssunto, String strMensagem) {
		contatoPO.txtEmpresa.sendKeys(strEmpresa);
		contatoPO.txtNome.sendKeys(strNome);
		contatoPO.txtTelefone.sendKeys(strTelefone);
		contatoPO.txtEmail.sendKeys(strEmail);
		contatoPO.txtAssunto.findElement(By.xpath("//*[contains(text(), '" + strAssunto + "')]")).click();
		contatoPO.txtMensagem.sendKeys(strMensagem);
	}

	@And("^clicar no botao \\\"Enviar\\\"$")
	public void clicar_no_bot_o_Enviar() {
		contatoPO.btnEnviar.click();
	}

	@Then("^o site ira registrar o envio apresentando uma mensagem de que a mensagem foi enviada com sucesso\\.$")
	public void o_site_ir_registrar_o_envio_apresentando_uma_mensagem_de_que_a_mensagem_foi_enviada_com_sucesso() throws InterruptedException {
		String txt = "Sua mensagem foi enviado com sucesso!";
		while (contatoPO.msgEnviado.getText().toLowerCase().contains("carregando")) {
			Thread.sleep(1000);
		}
		Assert.assertTrue(contatoPO.msgEnviado.getText().toLowerCase().contains(txt.toLowerCase()));
	}

}
