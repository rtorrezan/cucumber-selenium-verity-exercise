package br.com.verity.runner;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;

import net.serenitybdd.cucumber.*;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = "features", 
				 glue = { "br.com.verity.stepdefinitions" },
			     monochrome = true,
			     tags = {"~@Ignore"},
			     plugin = {"pretty", "html:target/cucumber-html-report"}
			     
		
		)

public class TestRunner {
    public static void main(String[] args) throws Exception {
        // SpringApplication.run(TestApplication.class, args);
        JUnitCore.main(TestRunner.class.getCanonicalName());
    }
	public TestRunner() {

	}

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {

	}

	@Test
	public void test() throws Exception {
	}
}