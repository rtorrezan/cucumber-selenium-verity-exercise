Feature: Realizar cadastro de Contato no Site

  Scenario Outline: Realizar cadastro de contato com sucesso
	Given que eu acessei a home page da Verity Group
	When eu clicar na opcao de menu "Contato"
	And preencher os campos Empresa: "<empresa>", Nome: "<nome>", Email: "<email>", Telefone: "<telefone>", Assunto: "<assunto>" e Mensagem: "<mensagem>"
	And clicar no botao "Enviar"
	Then o site ira registrar o envio apresentando uma mensagem de que a mensagem foi enviada com sucesso.


    Examples: 
    |empresa      	| nome 						| email 				| telefone  	| assunto 		| mensagem 		|
  	| Rtorrezan		| ricardo silva torrezan	| rtorrezan@gmail.com	| 11981012393	| Dúvidas		 | teste vaga QA |